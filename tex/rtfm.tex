%!TEX root = ../main.tex

\lettrine{O}{ne se} le cachera pas : \LaTeX{} est ingrat. Sa complexité est sans limites et en avoir une bonne maîtrise suppose d'y consacrer un temps que le commun des mortels préfère réserver à autre chose (comme faire de la science). Certaines personnes ont néanmoins fait l'effort d'investir du temps dans \LaTeX{}, et ce sont elles qui ont créé ce logiciel de composition, programmé les classes et les extensions que nous allons utiliser et écrit des livres pour expliquer comment s'en servir. Nous allons donc voir comment accéder à de la documentation de qualité sur \LaTeX{} et ses extensions pour ne pas perdre notre temps à apprendre comment les entrailles de \LaTeX{} sont faites.

% ================================================
\section{Démarrer avec \LaTeX{}}
% ================================================

Je te l'avais promis, cher lecteur : si tu ne connais pas \LaTeX{}, je t'indiquerai qui peut te dire comment t'en servir.
Il existe de très nombreux tutoriels pour se familiariser avec \LaTeX{}.
Je n'en citerai donc que quelques-uns, qui devraient très largement te suffire pour te lancer.
Choisis-en un et suis-le sérieusement.

\paragraph{OpenClassrooms.} \url{https://openclassrooms.com}

Ce site est dédié à la diffusion de cours, notamment sur les logiciels informatiques et les langages de programmation.
Le cours sur \LaTeX{} est très progressif : une première partie pose les bases, et une deuxième revient sur des points plus avancés.

\paragraph{Wikibooks.} \url{https://fr.wikibooks.org/wiki/Programmation_LaTeX}

Ce site, hébergé par la fondation Wikimédia, présente un cours géré de façon communautaire.
Le cours est également assez progressif.

\paragraph{Framabook.} \url{http://framabook.org/tout-sur-latex/}

Encore un très bon cours de base sur \LaTeX{}.

\paragraph{Documentation \og officielle \fg.} \url{https://www.latex-project.org/guides/}

Le site officiel de \LaTeX{} recense un certain nombre de tutoriels en anglais.


% ================================================
\section{Trouver de l'aide}
% ================================================

Même quand on a suivi un tutoriel, il arrive qu'on ne sache pas comment s'y prendre. 
On aimerait faire une chose exotique qui n'est pas gérée par les commandes de base de \LaTeX{} : écrire de la chimie, faire des dessins avec des commandes simples, faire des graphiques, etc.
Il peut aussi arriver qu'on sache qu'une extension résoudra un problème de l'on rencontre, mais qu'on ne sache pas comment elle fonctionne.

Pour tous ces problèmes, la solution se trouve sur Internet (et éventuellement dans quelques livres, mais je suis à peu près certain que tu n'as pas de livre sur \LaTeX{} sur ton bureau, cher lecteur).
Mais encore faut-il savoir quoi chercher et comment le trouver.

\paragraph{texdoc.}

La commande \texttt{texdoc} permet un accès rapide à la documentation de la distribution \LaTeX{} installée sur son système.
Je n'ai pu l'utiliser que sur les systèmes Unix correctement configurés, mais je crois qu'elle doit aussi fonctionner sous Windows en faisant quelques manips.
Elle se contente d'ouvrir la documentation associée à un mot-clé qu'on lui donne en argument.
Par exemple, si je veux lire la documentation de l'extension Babel, je n'ai qu'à ouvrir un terminal et taper :
\begin{verbatim}
    texdoc babel
\end{verbatim}
Mon lecteur PDF s'ouvrira alors automatiquement et m'affichera la documentation de Babel, dans la version correspondant à celle de l'extension qui est installée sur mon ordinateur.

\paragraph{La documentation de la distribution \TeX.}

Si tu as été prudent et que tu as installé la totalité des composants de ta distribution \TeX{}, la documentation a été copiée sur ton disque dur. Ce sont ces ressources auxquelles accède la commande \texttt{texdoc}. L'index est accessible sous la forme d'un fichier HTML situé dans le répertoire d'installation de la distribution \TeX.

\paragraph{CTAN.} \url{https://ctan.org}

\textit{The Comprehensive \TeX{} Archive Network} est un site dédié à la distribution d'extensions et de logiciels pour \TeX{}.
Dans sa version actuelle, le site permet de faire des recherches pour trouver la documentation d'une extension.
Attention toutefois, cher lecteur : la documentation archivée pour une extension ne correspond pas forcément à la version qui est installée sur ton système.

\paragraph{\TeX Exchange.} \url{https://tex.stackexchange.com}

StackExchange est un site communautaire de questions-réponses.
Il est décliné pour différents sujets.
\TeX Exchange est sa déclinaison dédiée à \LaTeX{}.
On y trouve les réponses à des points précis, du genre : \foreignquote{english}{Prevent hyphenation and use line break in block of text}.
En cas de question sur la faisabilité technique d'une opération particulière, c'est un bon endroit où chercher.

\paragraph{Les moteurs de recherche.}

Google, Bing, Yahoo, Duck Duck Go... Les moteurs de recherche sont légion et répondent à beaucoup de questions. 
Ils agrègent les résultats des sources précédentes, et plus encore ; cependant, il arrive aussi que leurs réponsent soient incomplètes ou non pertinentes.
J'utilise les moteurs de recherche (j'en utilise plusieurs, car ils ne renvoient pas tous les mêmes résultats) quand l'ensemble des sources précédentes se sont avérées insuffisantes.

% ================================================
\section{Saines lectures}
% ================================================

Certains ouvrages ne sont pas des tutoriels, mais les lire ou les avoir sous la main peut présenter un très grand intérêt.

\paragraph{The Comprehensive \LaTeX{} Symbol List.} \url{https://www.ctan.org/tex-archive/info/symbols/comprehensive}

Une liste assez complète des symboles utilisables avec \LaTeX{}. On en a toujours besoin, je t'assure ! Également accessible par \texttt{texdoc} : 
\begin{verbatim}
    texdoc symbols-a4
\end{verbatim}

\paragraph{DeTexify.} \url{http://detexify.kirelabs.org}

Sans doute la meilleure façon de trouver le symbole \LaTeX{} qu'on cherche : on trace sa forme à la souris et le site recherche les symboles qui ressemblent le plus. Il donne la commande correspondante et l'extension à inclure si nécessaire.

\paragraph{Les fiches à Bébert.} \url{http://bertrandmasson.free.fr}

Le mainteneur de ce site est issu des sciences humaines.
Il a écrit des tutoriels qu'il appelle \og fiches \fg, et dans lesquels il explique pas mal de choses (mais rien sur les maths, malheureusement).
Le ton légèrement humoristique des fiches les rend plaisantes à lire.
Le site est en cours de refonte, mais la nouvelle version n'est pas aussi agréable à lire que les bons vieux PDF qu'on peut télécharger sur l'ancienne.

\paragraph{L'Orthotypograhie.} \url{http://www.orthotypographie.fr}

Bien taper ses textes, ce n'est pas seulement écrire dans une langue grammaticalement et orthographiquement correcte.
C'est également effectuer correctement la typographie.
Mettre les bonnes espaces au bon endroit, les capitales là où il y en a besoin, etc.
Ce livre, édité d'après l'\oe uvre d'un dénommé Jean-Pierre \textsc{Lacroux}, est une compilation d'articles sur les usages typographiques que ce derbier considère comme corrects (en français).



