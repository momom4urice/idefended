%!TEX root = ../main.tex

\lettrine{T}{ravailler dans} de bonnes conditions, c'est mettre toutes les chances de son côté pour se concentrer sur l'essentiel.
Un environnement de travail correctement configuré éviter les petits soucis énervants, du genre \enquote{Je comprends pas pourquoi PDF\LaTeX{} se lance pas !} ou bien \enquote{Pourquoi les commandes de siunitx marchent plus depuis que j'ai mis ma distribution \TeX{} à jour ?}.

\section{Choisir et maintenir une distribution \TeX{}}

On a tendance à l'oublier quand on commence à le faire, mais rédiger sa thèse, ça se fait 24~h/24 et 7~j/7.
En tous lieux, à toute heure, on rédige.
Et on n'a pas toujours le privilège de se voir équipé d'un ordinateur portable par son laboratoire.
Dans des cas terribles, on doit faire cohabiter plusieurs plate-formes logicielles : Mac OS au travail, Windows à la maison, Linux sur l'ordinateur portable...
Or, les distributions \TeX{} ne sont pas identiques partout.
Nous allons donc voir ici comment sélectionner sa distribution \TeX{} et comment la maintenir à jour pour éviter des déconvenues lorsqu'on essaye de compiler son document sur une machine différente de la sienne.

\subsection{Sous Linux}

Les distributions Linux incluent presque toutes une version de la distribution \TeX{} Live (Debian, Ubuntu, SuSE, Fedora...).
L'installation des paquets de \TeX{} Live est généralement simple.
Sous les distributions dérivées de Debian, il suffit d'ouvrir un terminal administrateur et de taper :
\begin{verbatim}
    apt-get install texlive-full
\end{verbatim}
Pour les autres distributions, je ne sais pas comment faire, mais une petite recherche sur Internet te dira comment t'y prendre, cher lecteur !

Dans tous les cas, prends bien garde à vérifier quelle version de \TeX{} Live est fournie.
C'est très important : certaines extensions évoluent d'une version à l'autre, et cela peut parfois introduire des incompatibilités problématiques (bien que la plupart des extensions soient conçues avec un objectif de rétro-compatibilité).

\TeX{} Live est divisée en plusieurs paquets, et on peut être tenté de n'en installer qu'une partie. 
Tu peux le faire, mais si tu n'as pas d'économie de bande passante ou d'espace disque à faire, je te recommande d'installer la totale : cela t'évitera des soucis.

La mise à jour de \TeX{} Live se fait en même temps que celle du système d'exploitation.
Debian a un cycle de développement long, et il arrive que \TeX{} Live ne soit pas mis à jour pendant plusieurs années de suite.
Ubuntu hérite de ce cycle de développement long, et on se retrouve parfois dans des situations gênantes, comme un Ubuntu 12.04 équipé d'un \TeX{} Live vieux de trois ans, dans lequel manquent plusieurs de mes extensions préférées.
Il existe, pour ces cas, des dépôts logiciels de substitution, qui proposent une version de \TeX{} Live plus récente que celle présente dans les dépôts logiciels par défaut du système d'exploitation.

\subsection{Sous Mac OS X}

\TeX{} Live est également disponible sous Mac OS X sous le nom de Mac\TeX. 
Le téléchargement et l'installation sont triviaux.
Mac\TeX{} est également disponible par Fink et MacPorts.
Il est enfin possible de télécharger les anciennes versions de Mac\TeX{}, \href{http://tex.stackexchange.com/questions/65674/how-to-download-mactex-2011-now-that-mactex-2012-is-available}{comme on peut le lire sur \TeX Exchange}.

\subsection{Sous Windows}

Le monde UNIX est marqué par l'hégémonie quasi-absolue de \TeX{} Live. Sous Windows, les choses sont toutes autres. 
\TeX{} Live existe, elle est maintenue et suit le cycle développement annuel des autres éditions. Les autres distributions populaires sont Mik\TeX{} et Pro\TeX t (qui est basée sur la précédente et dont je ne parlerai pas, faute d'expérience).

L'installation de \TeX{} Live est simple : il suffit de se rendre sur le site de la distribution, de télécharger la version Windows et de l'installer en suivant les instructions. Là encore, je recommande de tout installer pour ne pas avoir d'extension manquante à la compilation des documents. Il m'est arrivé d'avoir des soucis lors de l'installation de \TeX{} Live. Si des problèmes insolubles se posent à l'installation et que tu n'as pas toute ta vie pour installer \TeX, Mik\TeX{} est une solution alternative tout-à-fait satisfaisante.

Mik\TeX{} est, à mon avis, plus simple d'utilisation de \TeX{} Live. L'installation est au moins aussi simple que celle de \TeX{} Live. Cependant, cette fois, il n'est pas nécessaire d'installer tous les paquets de la distribution :  Mik\TeX{} est équipée de scripts qui permettent d'installer un paquet lorsque son contenu est nécessaire pour compiler le document. Attention cependant : il arrive que cela ne fonctionne pas.

Attention : les contenus de Mik\TeX{} et \TeX{} Live ne sont pas synchronisés. 
Cela peut conduire à des incompatibilités et rendre, en des occasions heureusement rares, un document non-compilable d'un ordinateur à l'autre.

\subsection{Sous Windows, avec Cygwin}

Peut-être es-tu utilisateur de Cygwin, cher lecteur ? Cygwin est un environnement Windows compatible POSIX (autrement dit, une espèce d'Unix qu'on peut installer dans Windows). Pour ma part, c'est ce que j'utilise : Cygwin permet de retrouver la grande majorité des fonctionnalités de la ligne de commande Linux sous Windows. On peut également installer \TeX{} Live dans Cygwin, soit en utilisant le programme de configuration \texttt{setup.exe}, soit en utilisant le script Perl d'installation pour les environnements Unix, fourni sur le site web de la distribution \TeX. J'ai une préférence pour la deuxième méthode, car les scripts de post-installation de \TeX{} Live pour Cygwin (la version fournie quand on utilise \texttt{setup.exe}) étaient buggés et pouvaient mettre plusieurs dizaines d'heures à accomplir leur tâche. Ça a peut-être été rectifié depuis, à vérifier.


\section{Intégrer sa distribution \TeX{} à son environnement}

Parfois, les scripts d'installation des distributions \TeX{} ne font pas les choses correctement. Les problèmes dont je vais parler ici sont très rares sous Linux, occasionnels sous Mac OS et fréquents sous Windows.

En fonction de la façon dont tu aimes travailler, tu pourras être amené à compiler tes documents en utilisant le terminal. (Si tu ne sais pas de quoi je parle, tu dois sans doute pouvoir te passer de tout ce que je suis sur le point de raconter.) Par exemple, pour compiler un document, on veut pouvoir taper
\begin{verbatim}
    pdflatex <document>
\end{verbatim}
Cela n'est possible que si les variables d'environnement du système d'exploitation sont correctement configurées.

Dans tous les systèmes d'exploitation courants (y compris Windows), il existe une variable d'environnement, nommée <PATH>, qui contient les chemins où l'OS va chercher les fichiers exécutables.
Si le binaire de PDF\LaTeX{} ne se trouve pas dans un répertoire dont le chemin n'est pas dans le <PATH> du système d'exploitation, entrer la commande \verb|pdflatex| dans un terminal ne fonctionnera pas. Or, il peut arriver que le <PATH> ne soit pas correctement configuré à l'installation de la distribution \TeX.

Résoudre le problème d'un <PATH> mal configuré est très simple.
L'\href{https://fr.wikipedia.org/wiki/Variable_d%27environnement}{article Wikipédia} sur les variables d'environnement contient toutes les informations nécessaires pour résoudre ce problème.
Sous Windows, le répertoire où se trouvent les exécutables de la distribution \TeX{} choisie se situe dans celui où celle-ci est installée (évident, n'est-ce pas ?) et qui est déterminé par l'utilisateur à l'installation (et je ne peux donc pas le deviner pour toi).
Sous Mac OS et Linux, il s'agit en général de \verb|/usr/texbin|.


\section{Choisir un éditeur de texte}

Cher lecteur, si tu as décidé d'utiliser \LaTeX{}, tu sais certainement que tu dois composer ton document en utilisant un éditeur de texte indépendant de ton compilateur. Je vais te donner quelques exemples de bons éditeurs de texte pour que tu ne passes pas une éternité à en chercher un bon.

Je n'aime pas les solutions intégrées, qui ont une fâcheuse tendance à présenter des défauts que je peux éliminer en utilisant des outils dédiés à chaque tâche. Je n'utilise donc pas d'environnement de développement intégré incluant une console de compilation ou une visionneuse PDF. Cependant, cher lecteur, je sais que tes goûts sont différents des miens. Les solutions que je propose sont toujours gratuites, car un thésard n'est pas bien fortuné et préfère garder son argent pour rapporter des souvenirs quand il va en conférence à l'étranger.

\subsection{Multi-plateformes}

Les éditeurs multi-plateformes ont ma préférence car ils me permettent de retrouver à l'identique mon environnement de travail, quel que soit le système d'exploitation sous lequel je me trouve. Les éditeurs de texte présentés ici sont tous disponibles sous toutes les plateformes courantes.
\begin{description}
  \item[\TeX Maker.] \TeX Maker est un environnement de développement \LaTeX{} intégré complet. Il permet de taper son document, de le compiler avec la distribution \TeX{} de son choix, de chercher les erreurs et de visualiser les documents. C'est certainement l'environnement \LaTeX{} multi-plateformes le plus populaire. Il est facile à utiliser et augmente la productivité de façon significative. Il gère l'UTF-8.
  \item[\TeX Studio.] \TeX Studio est un fork de \TeX Maker 2. Le panel de ses fonctionnalités est plus ou moins le même que celui de \TeX Maker, mais je préfère sa coloration syntaxique plus outrée.
  \item[LyX.] Tous les éditeurs que j'ai cités nécessitent que l'utilisateur tape directement le code du document. LyX est à mi-chemin entre un éditeur de texte brut et un éditeur WYSIWYG\footnote{\textit{What you see is what you get.}} (comme Word) : c'est un éditeur WYSIWYM\footnote{\textit{What you see is what you mean.}} qui remplace une partie du code \LaTeX{} par de la mise en forme. Ainsi, l'instruction \verb|\textbf{}| est interprétée comme une fonte grasse et s'affiche comme tel à l'écran. C'est un peu plus visuel que les éditeurs de texte simples, et les débutants pourraient préférer cela.
  \item[Sublime Text.] C'est mon éditeur de texte préféré. Il est simple, élégant, moderne, un peu geek et incroyablement puissant. Il implémente de nombreuses fonctionnalités présentes, par exemple, dans VIM, comme la sélection par blocs. Il permet l'édition simultanée de plusieurs parties d'un même fichier. Mais le plus important est qu'il gère \LaTeX{} à la perfection grâce à son extension \href{https://github.com/SublimeText/LaTeXTools}{\LaTeX Tools}. Il coûte 70~\$ ; ça fait un peu cher, mais ça en vaut la peine. Il peut être \og essayé \fg gratuitement, sans limitation dans le temps.
  \item[Vim, Emacs.] Les deux éditeurs de texte en console les plus populaires sont parfaitement capables de faire du \LaTeX. Je ne m'en sers jamais pour cela, mais ils disposent d'excellents plugins pour cette tâche.
\end{description}

\subsection{Sous Linux}

Linux, en raison de l'affection que lui portent les programmeurs et les bidouilleurs, offre des dizaines d'excellents éditeurs de texte gratuits. Je n'en ai testé qu'un nombre très réduit, et je n'en ferai donc pas une liste exhaustive.

\begin{description}
  \item[Gedit.] Il s'agit de l'éditeur par défaut de Gnome. Il comporte un plugin \LaTeX{} raisonnablement puissant.
  \item[Kile.] Le meilleur éditeur \LaTeX{} sous Linux à mon avis. Il utilise les bibliothèques KDE et s'intègre parfaitement dans cet environnement de bureau. Je le recommande tout autant pour une utilisation \og à l'ancienne \fg. Son interface est très intuitive, il propose des tonnes de macros pré-enregistrées, permet la gestion de projets pour les très gros documents comportant beaucoup de fichiers... Attention aux menteurs qui vous le vendront comme étant multi-plateformes : l'installer sous Windows est hasardeux, et c'est une purge sous Mac OS (du moins d'après mon expérience).
\end{description}

\subsection{Sous Mac OS X}

Je n'ai honnêtement pas trouvé de meilleure alternative que les solutions multi-plateformes que j'ai présentées. Néanmoins, par souci d'honnêteté intellectuelle, je mentionnerai une alternative crédible.

\begin{description}
  \item[\TeX Shop.] C'est l'éditeur par défaut de Mac\TeX. J'ai eu une très mauvaise expérience avec cet éditeur, sans doute à cause de son interface très \og Macisante \fg. Néanmoins, il a relativement bonne presse, donc j'imagine qu'il n'est pas si mauvais. Attention : il encode par défaut en Mac Roman. C'est un véritable fléau pour le travail en équipe, et je préfère le régler pour qu'il encode les nouveaux fichiers en UTF-8.
\end{description}

\subsection{Sous Windows}

Les éditeurs de texte sont abondants sous Windows. Le Bloc-notes, fourni par défaut avec le système d'exploitation est beaucoup trop simpliste pour être utilisé pour développer quoi que ce soit, et on ne saurait en aucun cas écrire un document \LaTeX{} avec. J'exagère à peine en disant cela. Quoi qu'il en soit, il existe des tas de solutions.

\begin{description}
  \item[Notepad++.] Il s'agit d'un simple éditeur de texte, basé sur la bibliothèque Scintilla. Il est très bien fait et possède certainement des extensions pour \LaTeX, mais j'avoue ne jamais m'en être servi pour cela.
  \item[\TeX nic Center.] C'est l'éditeur de texte par défaut de Mik\TeX. Il est riche en fonctionnalités, mais je ne le connais pas bien. Fais attention à bien vérifier l'encodage des fichiers qu'il produit (bien qu'il me semble qu'il produise par défaut de l'UTF-8).
  \item[\TeX Works.] \TeX Works reproduit le comportement de \TeX Shop (voir plus haut). Je n'ai pas essayé de m'en servir plus de cinq minutes, je n'ai pas aimé.
\end{description}

\section{Choisir une visionneuse PDF}

Quand on compose un document \LaTeX, on aime bien voir à quoi il va ressembler quand on va le compiler. Les vrais oufs visionnent les fichiers DVI de leurs documents ; cependant, aujourd'hui, le format PDF est devenu suffisamment populaire pour que les gens qui programment \TeX{} essayent de le supporter correctement. Je ne donnerai pas des tonnes d'alternatives pour les visionneuses PDF, car il en existe en réalité assez peu qui tiennent vraiment la route.

La visionneuse PDF qu'on utilise doit disposer de deux fonctionnalités essentielles : elle ne doit pas verrouiller le fichier qu'elle lit, et elle doit être capable de détecter automatiquement lorsqu'il est modifié. Pour cette raison, cher lecteur, tu ne dois pas utiliser Adobe Reader (qui verrouille le fichier qu'il lit). 

\subsection{Sous Linux}

\begin{description}
  \item[Evince.] La visionneuse de Gnome, n'en déplaise à certains fâcheux, fait un travail correct. Elle gère les signets, détecte les modifications et ne verrouille pas le fichier visionné.
  \item[Okular.] Comme la précédente, mais pour KDE. Elle était plus rapide qu'Evince il y a quelques années, mais les choses ont pu changer depuis.
\end{description}

\subsection{Sous Mac OS X}

\begin{description}
  \item[Aperçu.] La visionneuse par défaut de Mac OS X est raisonnablement équipée. Elle ne détecte pas les modifications, mais il suffit de mettre la fenêtre en arrière-plan en en sélectionnant une autre, puis de mettre à nouveau Aperçu en avant, pour que l'affichage soit rafraîchi. On n'en souffre pas trop.
  \item[Skim.] Pas d'hésitation à avoir : Skim est meilleur qu'Aperçu. Point final.
\end{description}

\subsection{Sous Windows}

\begin{description}
  \item[SumatraPDF.] La meilleure visionneuse PDF sous Windows pour faire du \LaTeX. Aucune autre alternative viable et populaire n'existe.
\end{description}

\section{Gérer sa bibliographie}

Une thèse n'est rien sans sa bibliographie, comme tu le sais, cher lecteur. Comme tu t'intéresses un peu à \LaTeX{}, tu sais qu'il vaut mieux préparer sa bibliographie avec Bib\TeX{} pour pouvoir ensuite la mettre en forme à son gré. Toutefois, gérer un fichier Bib\TeX{} avec plusieurs dizaines, voire des centaines d'entrées, peut être un peu fastidieux. Le thésard moderne et futé peut même vouloir générer la bibliographie de sa thèse à partir de sa bibliographie de recherche. Il existe pour cela quelques outils, certains étant dédiés à l'édition de fichiers Bib\TeX{}, et d'autres étant d'abord et avant tout des gestionnaires de bibliographie, dotés d'un export Bib\TeX.

Les outils présentés ici sont généralement incompatibles entre eux, mais sont tous très puissants.


\subsection{Multi-plateformes}

\begin{description}
  \item[JabRef.] JabRef est un éditeur de fichier Bib\TeX{} écrit en Java. Il permet de gérer une bibliographie et sauvegarde directement ses données au format Bib\TeX. On peut y inclure des liens vers les PDF associés aux documents que l'on a enregistrés.
  \item[Zotero.] Zotero est une extension pour Firefox qui permet de gérer sa bibliographie. Il propose un export Bib\TeX{} et extrait les données des pages web pour ajouter sans difficulté des entrées à ta base bibliographique. La bibliographie est stockée dans le cloud, si bien que tu peux la retrouver partout.
  \item[Mendeley.] Cette application est écrite en Qt et est parfaitement portable sur toutes les plateformes. C'est un excellent gestionnaire de bibliographie, qui stocke également ses données dans le cloud. Il possède un export Bib\TeX{}, un plugin Word et il peut importer les métadonnées d'une page web ou de certains PDF. C'est ma solution favorite, bien qu'il s'agisse d'un logiciel non-libre publié par Elsevier.
\end{description}

\subsection{Mac OS X}

\begin{description}
  \item[BibDesk.] C'est comme JabRef, avec une meilleure interface et une excellente intégration système. Malheureusement, cette application n'est pas multi-plateformes, et c'est précisément pour cela que je ne m'en sers pas et que je lui préfère Mendeley.
\end{description}





