%!TEX root = ../main.tex

\lettrine{A}{h ! les} joies de la thèse ! 
Une thèse est comme les encadrants qui la dirigent : on passe bien souvent autant de temps à la maudire qu'à l'aimer. 
La thèse, c'est le Mont-Blanc du doctorant, le sommet à gravir, en haut duquel on ne croit pas arriver, mais au sommet duquel on se trouve tout de même un beau matin. 
On y passe des semaines, voire des mois, carburant au café, au thé.\footnote{On raconte même que certaines thèses auraient été écrites sous cocaïne.}
Avant la remise au jury, on s'enferme à double-tour avec une cafetière et des sandwichs, et on y passe 180~heures d'affilée.
Et finalement, on accouche du mémoire, et on l'envoie à son jury.
Et on repense à ce qu'on a enduré pendant tout ce temps.
Au nombre de fois que ce satané \LaTeX{} n'a pas fait ce qu'on voulait.
Au nombre de fois que ses encadrants ont réclamé des modifications qu'on ne savait pas faire : \og aligne-moi le tableau comme-ci \fg, \og place-moi cette figure comme ça \fg, \og fais ta biblio comme-ci \fg.
Et au nombre de fois qu'on a maudit le monde entier pour nous avoir donné un \textit{template} de thèse pas exactement comme on voulait.

Car il est courant chez le doctorant de ne pas vouloir s'encombrer des considérations techniques de \LaTeX{} pour se concentrer sur son travail.
Ça ne vous étonne pas, j'imagine.
Mais le pauvre doctorant, ignorant qu'il est de ce qui a été mis dans son \textit{template}, ne sait pas toujours comment le modifier.
Il ne sait pas toujours comment changer les marges de son manuscrit.
Il ne sait pas toujours comment remplacer la sempiternelle et ô combien galvaudée fonte Computer Modern par quelque chose d'un peu plus sexy.
Il ne sait pas qu'il exaspère le jury sans le vouloir avec une typographie approximative parce qu'il n'a pas utilisé l'extension adaptée pour taper ses formules chimiques.
Et il ne sait pas des quantités de choses avec lesquelles il n'a pas envie d'encombrer son esprit.
Il compose laborieusement son texte, repassant avec soin ses hypothèses et pestant contre cette \og saloperie d'usine à gaz de \LaTeX{} \fg.
Et à la fin, il regarde son travail et la dure réalité est là :
\begin{center}
\setlength{\parindent}{0cm} \Large \textsc{Son mémoire est laid.}
\end{center}
Du moins, il n'est pas à son goût, et il n'est pas complètement satisfait de la tête de sa thèse.

Mon avis est que la thèse, en tant qu'aboutissement ultime des études du doctorant, doit être aussi parfaite que possible sur tous ses aspects. 
Celui de la science, mais aussi celui de l'esthétique.
Il a consacré trois années à ce travail, et il doit pouvoir le regarder avec fierté.
Mon avis est également que les \textit{templates} clés-en-main ne sont jamais parfaitement calibrés pour les usages de chacun. 
L'adaptation parfaite aux besoins et aux désirs du rédacteur, ainsi que des conflits entre extensions, peuvent nécessiter d'aller fouiller dans les tréfonds du \textit{template} pour que tout fonctionne bien.
On touche alors les limites du concept : à quoi bon avoir un superbe \textit{template} si c'est pour aller le modifier par la suite ?
On voit sans arrêt des doctorants pleurer sur les forums après le \textit{template} de thèse de leur université, qui n'est pas à jour, qui ne prend pas tel ou tel codage, qui ne permet pas d'utiliser telle ou telle extension...

L'apprentissage de \LaTeX{} est long et difficile. 
Et à vrai dire, les véritables connaisseurs de ce logiciel sont peu nombreux.
En revanche, les utilisateurs astucieux sont légion, et il est relativement aisé d'en devenir un.
Il faut simplement se donner le temps de faire les choses soi-même et de construire son document en fonction de ses besoins.

Lorsque j'ai rédigé ma thèse, je suis parti de presque zéro parce que je n'aimais pas l'esthétique du \textit{template} de mon laboratoire.
J'ai donc sélectionné la classe la plus puissante que je connaissais et j'ai écrit intégralement mon préambule.
J'ai ainsi pu contrôler parfaitement de ce que j'ai fait.
Et quand on m'a demandé \og mon \textit{template} de thèse \fg, je me suis posé la question : fallait-il que je livrasse%
\footnote{L'imparfait du subjonctif est d'un pédantisme absolu, mais il faut rester rigoureux : on ne transige pas avec la langue française. En cas de doute : \url{http://www.conjuguetamere.com}.} %
une solution clés-en-main ?
Je me suis dit que ça n'avait pas d'intérêt et que je n'avais pas envie de me casser la tête à faire joli \textit{template} super compliqué qui allait planter chez un thésard sur deux.

Cher lecteur,%
\footnote{Je n'oublie pas les lectrices, cher lecteur ; mais le français a ceci d'ingrat qu'il est épouvantablement machiste et que les substituts d'usage permettant de ne pas passer pour un ignoble misogyne sont peu élégants. Que dirais-tu de mon texte, cher lecteur/chère lectrice, chè/er(e) lecteur/lectrice, ch(è|e)r$\cdot$e lect(eur|rice), si j'insérais ces ridicules formules à tout bout de champ ?  Je proposerai une version féminisée de ce texte quand j'aurai du temps à tuer en programmant tout ça avec des macros. Je vous le promets, mesdames.}%
{} ce document est donc un manuel.
C'est moins bien qu'un \textit{template}, parce que tu ne pourras pas obtenir une thèse identique à la mienne en deux minutes.
Mais c'est aussi beaucoup mieux qu'un \textit{template}, parce que tu sauras comment faire ce que tu as envie de faire.
Ce document décrit non seulement quelques astuces pour réaliser la thèse de tes rêves, mais il donne aussi des méthodes de travail et d'organisation qui rendront ta vie infiniment plus simple que la mienne a pu l'être.
Tu apprendras, par exemple :
\begin{itemize}
  \item que Bib\TeX{} est une vieille cochonnerie bonne pour la ferraille dans ce monde internationalisé à outrance ;
  \item que ton pire cauchemar sera les encadrants trop zélés qui veulent modifier eux-mêmes ton manuscrit ;
  \item que tu peux taper des unités et des nombres élégants sans avoir à passer trois heures à bidouiller des équations immondes en mode mathématique ;
  \item que tu peux faire une thèse de moins de 50~Mo si tu sais avec quoi faire tes graphiques ;
  \item qu'il existe une classe \LaTeX{} quasiment omnipotente et que tu vas regretter de ne pas avoir connu son existence plus tôt ;
  \item et tant d'autres choses encore...
\end{itemize}
Tu ne seras pas un professionnel de \LaTeX{}, pas plus que je n'en suis un moi-même, mais tu sauras comment être un utilisateur rusé et organisé qui ne perd pas un temps infini pour des trivialités.
Et comme je ne suis pas totalement une raclure, cher lecteur, je te donnerai aussi un semblant de \textit{template}. 
Un petit bout de projet sur lequel tu pourras construire ton propre document.

Je dois enfin t'avertir, cher lecteur : tu dois être un peu débrouillard.
Tu dois avoir suivi un semblant de formation de base à \LaTeX{} (je t'expliquerai où en trouver une si tu en as besoin).
Tu dois aussi être capable de lire de la documentation, car jamais je ne proposerai de manuel pour une extension ou l'autre : leurs documentations respectives le feront toujours mieux que moi.

\clearpage 

Voilà, cher lecteur. 
Si tu as pris le temps de lire cet avant-propos, sache que je respecte ton courage.
Et maintenant, place à \LaTeX{} !

\vskip2em
\begin{flushright}
--- Vincent
\end{flushright}

