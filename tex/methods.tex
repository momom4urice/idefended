%!TEX root = ../main.tex

\lettrine{P}{lus qu'un} bon \textit{template}, le thésard rédigeant a besoin de bonnes méthodes de travail. Tu n'imagines pas, cher lecteur, le temps qu'on peut perdre en s'organisant mal. J'en ai perdu pour deux, et je vais ici te donner quelques méthodes qui devraient t'aider un peu à t'organiser et, avec un peu de chance, te permettre de perdre un peu moins de temps.

\section{Structurer son projet}

% Organisation du dossier

On bon manuscrit est organisé. On y trouve facilement tout ce qu'on y cherche, et les différents types de contenu sont correctement séparés. J'organise l'arborescence de mes documents \LaTeX{} comme suit :
\dirtree{%
  .1 /.
  .2 fig/.
  .3 chapter\_un\_chapitre.
  .4 une\_figure.pdf.
  .4 une\_autre\_figure.pdf.
  .4 une\_autre\_figure.tex.
  .4 encore\_une\_figure.jpg.
  .3 chapter\_un\_autre\_chapitre.
  .4 une\_figure.png.
  .4 une\_autre\_figure.svg.
  .4 une\_autre\_figure.pdf.
  .4 encore\_une\_figure.pdf.
  .2 tex/.
  .3 abstract.tex.
  .3 appendix\_une\_annexe.tex.
  .3 body.tex.
  .3 chapter\_conclusion.tex.
  .3 chapter\_intro.tex.
  .3 chapter\_un\_chapitre.tex.
  .3 chapter\_un\_autre\_chapitre.tex.
  .3 nomenclature.tex.
  .3 preamble.tex.
  .3 acknowledgments.tex.
  .3 title.tex.
  .2 draft.tex.
  .2 main.tex.
}

À la racine, nous trouvons donc les documents \LaTeX{} que nous compilerons directement. Pour compiler cette thèse, il faut donc exécuter la commande
\begin{verbatim}
    pdflatex draft
\end{verbatim}
ou bien
\begin{verbatim}
    pdflatex main
\end{verbatim}
\texttt{draft.tex} permet de compiler le document en mode brouillon, et \texttt{main.tex} le compile en mode normal.

Le répertoire \texttt{tex/} comprend tous les documents \LaTeX{} contenant le texte de la thèse. Il s'agit donc du texte des chapitres, des annexes, de l'introduction, de la conclusion, des remerciements, etc. Je recommande de définir la structure de la thèse dans un fichier \texttt{body.tex}, qui inclura lui-même les autres fichiers. Je recommande également de mettre le préambule du document dans un fichier dédié, inclus lui-même dans \texttt{body.tex} ou dans l'un des fichiers à la racine (ici, \texttt{main.tex} ou \texttt{draft.tex}). On notera que les fichiers des chapitres sont nommés d'après leur contenu, et non d'après leur ordre dans le manuscrit.

Le répertoire \texttt{fig/} comprend des sous-répertoires associés à chaque chapitre, dans lesquels on trouve ensuite les originaux des figures ainsi que (si nécessaire) les exports permettant leur inclusion dans le document. Ainsi, en sauvegardant le document, on ne prend pas le risque de perdre les sources des figures.

\section{Compiler son document}

% latexmk, releases différenciées, PDFLatex (et PAS LATEX TOUT COURT, OMG)

La compilation du document est une étape primordiale. On peut l'effectuer de deux façons : soit en laissant le soin à un logiciel d'appeler les bonnes commandes, soit en le faisant soi-même.

\subsection{Comment compiler}

Appeler les commandes de compilation se fait en console. Il suffit de se placer dans le répertoire du document à compiler, et de taper
\begin{verbatim}    pdflatex <document>\end{verbatim}
Pour compiler la bibliographie, si on utilise Bib\TeX{}, on tape
\begin{verbatim}    bibtex <document>\end{verbatim}
Et ainsi de suite. Cette méthode présente toutefois des inconvénients : la résolution des références nécessite plusieurs compilations, et les commandes doivent alors être répétées plusieurs fois (généralement, deux fois suffisent). La bibliographie nécessite aussi une compilation. Un index et une nomenclature en nécessitent aussi une. La séquence de compilation est relativement simple, mais l'exécuter fréquemment devient vite fastidieux.

C'est pourquoi je préfère, même en console, avoir recours à un outil pour automatiser la séquence de compilation. Ainsi, seules les opérations nécessaires sont exécutées, et elles sont faites dans le bon ordre.
\begin{description}
    \item[GNU Make.] Make est l'outil de compilation le plus répandu. Il faut alors écrire un makefile pour préciser à Make quelles sont les règles et les séquences de compilation du projet. Écrire un makefile n'est pas une chose facile quand on ne connaît pas bien Make, et il existe des outils spécialement dédiés à \LaTeX{} pour faire le même travail sans se casser la tête.
    \item[latexmk.] latexmk est un script Perl automatisant la compilation à la façon de Make. Il est capable de rechercher les dépendances du document compiler. Pour effectuer toutes les opérations de compilation pour le document \texttt{main.tex} (bibliographie et index compris), il suffit de taper
    \begin{verbatim}    latexmk -pdf <document>\end{verbatim}%
    C'est, selon moi, la façon la plus agréable de procéder. latexmk est inclus dans toutes les distributions \LaTeX{} précedemment mentionnées.
    \item[arara.] Ce script remplit une fonction analogue aux deux programmes précédents. Je ne m'en suis cependant jamais servi.
\end{description}

On peut enfin déléguer la compilation à son éditeur de documents. La plupart des éditeurs \LaTeX{} ont un raccourci clavier pour la compilation, ce qui a pour effet de lancer la séquence correcte. Je n'irai pas plus loin dans les détails, car il faudrait alors écrire un manuel pour chaque éditeur, ce qui n'est pas la vocation de ce petit papier.

\subsection{Niveau de finition}

La génération du document en mode normal peut être très longue s'il contient des images ou des graphes TikZ ou PGFPlots complexes. Parfois, il peut être préférable de ne pas inclure tout le contenu graphique : on n'a pas besoin de toutes ces images pour vérifier qu'on a correctement entré une équation.

L'option \texttt{draft}, supportée par la plupart des classes \LaTeX{}, est destinée à cet usage. Cependant, trifouiller son document à chaque fois qu'on veut changer de mode de compilation est peu commode. C'est précisément pour cela que j'utilise deux fichiers maîtres, nommés respectivement \texttt{draft.tex} et \texttt{main.tex}. Ces deux fichiers contiennent seulement la commande d'appel de la classe utilisée, avec les options appropriées. Ils incluent ensuite le contenu de la thèse. Ensuite, selon que l'on souhaite compiler en mode normal ou brouillon, il suffit de compiler l'un ou l'autre des fichiers maîtres. On remarquera que l'option \texttt{draft} fait aussi figurer dans le document des marques mettant en valeur les problèmes de mise en page.


\section{Chercher des erreurs}

\og Ça compile pas ! \fg Mais pourquoi ? Cher lecteur, retiens ceci :
\begin{center}
\setlength{\parindent}{0cm} \Large \textsc{Tu ne dois pas laisser d'erreur de compilation.}
\end{center}
Quand la console te dit \og error \fg, tu arrêtes d'écrire et tu cherches pourquoi ça ne compile pas, même si la compilation a produit un PDF qui te semble vaguement correct. Laisser des erreurs de compilation non critiques, c'est la garantie d'avoir de très gros problèmes pour débugger lorsqu'une erreur critique pointera le bout de son nez.

Comme mentionné précédemment, il est préférable de travailler en compilant en mode brouillon : un temps de compilation plus court permet de compiler plus souvent, et donc de détecter des erreurs plus tôt.

\section{Travailler en équipe}

Un papier ne s'écrit généralement pas tout seul. Ton ou tes directeurs de thèse participeront à tes publications de plus ou moins loin. Certains participent aussi à la rédaction du manuscrit. Lorsqu'on travaille seul, il est facile de se coordonner avec les autres acteurs du projet : il n'y en a aucun. En revanche, échanger les modifications concurrentes effectuées sur un document lorsqu'on travaille à plusieurs peu rapidement devenir un cauchemar. Nous allons voir pourquoi, et je vais t'expliquer comment tu peux remédier à ces problèmes et pourquoi tu n'y arriveras peut-être pas.

\subsection{Un scénario classique}

Avec Internet est arrivé un outil formidable : le courrier électronique. Les génies qui l'ont conçu ont permis d'attacher des pièces jointes aux courriels. Ceci, cher lecteur, est ton pire cauchemar.

Imaginons que l'hypothétique doctorant Richard Feynman, qui travaille sous la direction du non moins hypothétique John Wheeler, commence à rédiger sa thèse sur le principe de moindre action. Le professeur Wheeler est un homme un peu dirigiste, et il préfère corriger lui-même le manuscrit de son thésard. Il a donc demandé à Rick (on lui donne son petit nom, il est dans le même labo que nous) de lui envoyer les parties de son manuscrit pour qu'il les corrige. Rick envoie donc le code source de son introduction à Wheeler par e-mail. Wheeler touille le chapitre de son côté, mais comme il est un peu débordé, il met du temps à le corriger. Rick, pendant ce temps, finit un chapitre de thèse ; il a par ailleurs trouvé des fautes d'orthographe dans son introduction et, plus important, il a décidé d'inclure de nouvelles choses.

Là, tu vois déjà le problème, cher lecteur. Premièrement, les modifications de Wheeler sont concurrentes de celles de Rick : le thésard va devoir intégrer ses modifications au document produit par son directeur, ou inversement. Deuxièmement, les modifications de Rick ne seront visibles par Wheeler qu'à l'itération suivante, ce qui va obliger Wheeler à relire toute l'introduction (qu'il a déjà corrigée) ou forcer Rick à détailler point par point les zones de l'introduction qu'il a modifiées.

Ces situations, et leurs innombrables variations, deviennent encore plus complexes lorsque le nombre d'acteurs dans le scénario augmente. On arrive donc, par exemple, à de déplaisants : \og Mais... Vous n'avez pas soumis la version que j'avais envoyée l'autre jour ? J'avais changé une figure, les unités n'étaient pas bonnes ! \fg Tout ça parce qu'un des rédacteurs, à un moment, a oublié d'intégrer une modification dans sa version du manuscrit en cours d'édition.

\subsection{Wheeler ne coopère pas}

Dans ce scénario, le professeur Wheeler préfère se contenter des échanges de mails parce qu'il sait le faire. Rick doit néanmoins optimiser son temps de travail. Quand il reçoit des modifications faites par Wheeler et concurrentes des siennes, il veut savoir où les différences se trouvent. 

Il peut tout d'abord voir où se situent les différences dans le document final en utilisant le script latexdiff. Ce script Perl compare deux fichiers \LaTeX{} et en produit un troisième sur lequel les différences sont visibles lorsqu'il est compilé. latexdiff possède quelques options ajustables par l'utilisateur.

Rick peut aussi visualiser les différences dans le code entre les différentes version des fichiers qu'il a en sa possession. Il a, pour cela, le choix entre de nombreux logiciels. On citera, par exemple : WinMerge (Windows), Meld (Windows et Linux), kdiff3 (Windows, Linux et Mac OS X), Diffuse (Windows, Linux et Mac OS X), Kompare (Linux), Tkdiff (Linux), etc.

Quoi qu'il en soit, ces méthodes ne sont que des tentatives pour combler les horribles failles d'une méthode de travail collaboratif fondée sur l'échange de courriels. Cher lecteur, si possible, procède autrement.


\subsection{Wheeler coopère}

Dans ce scénario, Wheeler est disposé à investir un peu de temps dans l'apprentissage d'une méthode de travail collaboratif plus moderne (voire, dans les cas idéaux, il la connaît déjà). Le méthode de travail que Rick décide d'adopter est fondée sur l'utilisation d'un système de gestion de versions. Les programmeurs connaissent souvent assez bien ces logiciels, qui sont destinés à suivre et enregistrer les modifications effectuées dans un fichier. Il en existe beaucoup, et on ne citera que quelques-uns des plus populaires : Subversion, Git, Mercurial, Bazaar, etc.

Le laboratoire de Rick est équipé d'un serveur dédié à l'hébergement de dépôts Git. Rick et Wheeler utilisent donc tous les deux ce logiciel pour suivre les modifications apportées au le code de la thèse de Rick. Lorsqu'il rédige, Rick poste périodiquement ses modifications sur le dépôt Git dédié au projet ; de son côté, le professeur Wheeler fait de même. Lorsque des modifications concurrentes sont introduites, Git prévient le collaborateur qui soumet le dernier et lui enjoint de régler les conflits. Lorsque Wheeler a effectué ses modifications et qu'il les a soumises, Rick peut alors les récupérer, et Git l'aidera à les intégrer à sa version du document s'il y a eu des modifications conflictuelles.

Je ne te ferai pas un cours sur les systèmes de gestion de versions, cher lecteur : ce serait beaucoup trop long, on peut en faire plusieurs livres. \href{http://fr.wikipedia.org/wiki/Gestion_de_versions}{L'article Wikipédia sur le sujet} est suffisant pour saisir les bases.

Quelle que soit la méthode de gestion de version que tu souhaites utiliser, je te recommande, cher lecteur, d'utiliser un logiciel de gestion de versions décentralisé tel que Git, Mercurial ou Bazaar. Le choix de l'une ou l'autre de ces solutions est surtout fait sur des critères pratiques, car ces trois exemples ont des fonctionnalités proches. Git est performant, très répandu et très bien documenté. Mercurial est moins répandu mais il est également bien documenté et il dispose plus ou moins des mêmes clients graphiques que Git. Bazaar, lui, est plus rare, mais il présente l'intérêt de pouvoir être utilisé dans une méthode centralisée sans être installé sur l'ordinateur hôte du dépôt ; son client graphique intégré est également d'assez bonne qualité.

\subsection{Codage des fichiers}

Les problèmes de codage, s'ils tendent à se raréfier avec l'adoption progressive de l'UTF-8, restent assez gênants quand ils se produisent. Rappelons que le codage d'un fichier texte est la représentation numérique des caractères d'écriture. Le codage courant le plus simple est le standard ASCII, qui ne prend en charge que 95 caractères. Au fil des années, de nombreux codages ont été créés pour supporter un nombre croissante de caractères. Il y a quelques années, les codages les plus couramment utilisés pour les langues européennes étaient définis par les standards ISO-8859. On trouvait également les codages CP1252, utilisé par Windows, et MacRoman, utilisé par Mac OS. La plupart de ces codages sont fondés sur des jeux de caractères différents et leur multiplication pose des problèmes assez évidents lors d'un travail collaboratif : quel codage utilisent les différents collaborateurs ? Le codage par défaut variant en fonction de l'éditeur de texte utilisé, il peut devenir difficile de savoir lequel doit être utilisé lors de l'ouverture d'un fichier. Ainsi, les caractères accentués peuvent se trouver mal interprétés, par exemple.

Le codage UTF-8, fondé sur le jeu de caractères Unicode, qui regroupe tous les jeux de caractères en usage à l'époque de sa définition, est aujourd'hui le standard qu'il est préférable d'utiliser. Il est possible d'écrire dans n'importe quelle langue en UTF-8 (ou presque). Tous les éditeurs de texte Linux encodent par défaut les fichiers en UTF-8 ; certains éditeurs sous Windows encodent toujours en CP1252, et certains sous Mac OS encodent en MacRoman. Il convient de rectifier cela dès qu'on s'en aperçoit afin d'éviter des déconvenues.

Attention : l'utilisation d'un codage autre que ASCII pour rédiger un document \LaTeX{} nécessite l'inclusion d'extensions. Bib\TeX{} ne gère pas l'UTF-8 (et c'est pour cela que je lui préfère Biber).