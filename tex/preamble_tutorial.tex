%!TEX root = ../main.tex

\lettrine{N}{ous arrivons} enfin à la partie que tu attendais, cher lecteur. Je vais te donner des recommandations pour la construction d'un préambule. Assure-toi de limiter le nombre d'extension intuiles que tu vas y inclure, car chaque extension supplémentaire est susceptible d'augmenter la durée de la compilation. C'est particulièrement critique sur les documents courts ; pour une thèse, c'est un peu moins gênant, dans la mesure où la compilation, même en mode brouillon, sera sans doute plus longue que le chargement des extensions en raison de la longueur du texte.

L'ordre de chargement des extensions peut avoir une importance. J'essayerai d'indiquer les conflits et l'ordre de chargement des extensions que je recommande.

\section{Choix de la classe}

Je recommande l'utilisation de la classe Memoir. La \href{http://www.ctan.org/pkg/memoir}{page CTAN} contient la documentation de la dernière version de la classe. Memoir intègre un code équivalent à de nombreuses extensions, dont la liste est fournie dans le très long manuel de la classe (près de 600 pages). Le chargement des extensions dont le code est automatiquement ignoré si on les inclut dans le préambule : tu n'as donc même pas de question à te poser à ce sujet.

Ce que j'aime à propos de Memoir :
\begin{itemize}
	\item c'est une classe extrêmement flexible, qui permet de composer n'importe quel type de document ou presque ;
	\item il est possible de personnaliser la mise en forme du document assez facilement ;
	\item son manuel est très bien fait.
\end{itemize}

\section{Encodage, fontes et langues}

\paragraph{inputenc.} Cette extension permet d'encoder les fichiers sources comme on le souhaite. J'utilise naturellement l'option \texttt{utf8}. Il est \href{http://tex.stackexchange.com/questions/13067/utf8x-vs-utf8-inputenc}{recommandé} de ne pas utiliser l'option \texttt{utf8x}.

\paragraph{fontenc.} Cette extension permet l'affichage correct des caractères accentués. Sans lui, un \og é \fg sera représenté comme un \og e \fg surmonté d'une apostrophe. Bien que la différence ne soit pas visuellement critique, les choses se corsent quand on fait une recherche ou un copier-coller dans un PDF mal encodé. On chargera cette extension en utilisant l'option \texttt{T1}.

\paragraph{lmodern.} Latin Modern est une fonte de substitution au traditionnel Computer Modern de Donald Knuth (le créateur de \TeX). Les modifications sont très légères, mais le plus important est qu'elle supporte plus de caractères accentués.

\paragraph{mathdesign.} Il s'agit d'une extension permettant de modifier les fontes mathématiques. mathdesign inclut tous les symboles d'amssymb et amsmath. Elle permet également de charger trois fontes avec support complet des mathématiques : Bistream Charter, URW Garamond et Adobe Utopia.

% \paragraph{typeface.} typeface est l'arme ultime pour la sélection des fontes. Cette extension permet la sélection de toutes les familles de fontes du document : roman, sans empattement, chasse fixe et mathématique. Elle charge automatiquement les extensions inputenc (option \texttt{utf8}) et fontenc (option \texttt{T1}). Elle permet également de charger lmodern et mathdesign avec diverses options. La documentation est très claire. \textit{Cette extension n'est cependant incluse que dans les versions très récentes (2012 corrigée et ultérieure) de \TeX{} Live.}

\paragraph{babel.} Indispensable, quelle que soit la langue dans laquelle on écrit (sauf, à la rigueur, quand on écrit dans la variante américaine de l'anglais). Cette extension permet notamment une gestion correcte de l'espacement et des coupures de mots en fonction de la langue sélectionnée. Il est possible de charger plusieurs langues, la dernière indiquée étant celle choisie par défaut. On peut ensuite changer la langue sélectionnée au moyen de la commande \texttt{\textbackslash selectlanguage}. On notera que l'option \texttt{french} est celle qui permet de composer un texte en français (pas de \texttt{frenchb} : tout a été normalisé). On remarquera que l'utilisateur moderne, qui compile ses documents avec Xe\LaTeX{}, préfèrera avoir recours à l'extension polyglossia (je ne fais pas partie de cette catégorie, et j'utilise toujours pdf\LaTeX{}).

\paragraph{csquotes.} Cette extension permet de typographier correctement les citations dans différentes langues.

\paragraph{microtype.} Cette extension améliore la micro-typographie. Je ne sais pas ce que c'est, mais ce dont je suis certain, c'est que les documents sont plus beaux avec que sans.

\paragraph{xspace.} Cette extension ajoute une commande qui insère une espace intelligente. Cela signifie qu'à la compilation, \LaTeX{} essaye de déterminer si une espace est nécessaire là où la commande \texttt{\textbackslash xspace} a été utilisée. L'intérêt est que le chargement de cette extension permet ensuite à babel d'utiliser \texttt{\textbackslash xspace} pour, par exemple, insérer correctement des espaces autour des guillemets.

\paragraph{lettrine.} C'est une question de goût ; je dois dire que je trouve qu'un chapitre a beaucoup plus d'allure quand il commence avec une jolie lettrine.

\section{Mathématiques et sciences}

\paragraph{amsmath.} amsmath définit plusieurs environnements mathématiques flexibles, fiables et logiques. Indispensable pour quiconque tape des équations mathématiques.

\paragraph{siunitx.} Cette extension permet de taper des unités et des nombres. Indispensable, tout simplement. Il faut prendre garder à sélectionner correctement la langue de cette extension au moyen de la commande 
\begin{verbatim}    \sisetup{locale = <symbole de la langue>}\end{verbatim}
La documentation explique comment synchroniser les changements de langue de siunitx et de babel.

\paragraph{mathbbol.} Support des fontes type tableau noir. Utile pour représenter les tenseurs, les ensembles mathématiques, etc.

\paragraph{empheq.} empheq permet d'encadrer des équations.

\paragraph{cancel.} Cette extension ajoute une commande permettant de barrer des termes dans les équations.

\paragraph{esvect.} Cette extension permet de représenter les vecteurs avec des flèches.

\paragraph{braket.} braket permet de représenter les vecteurs et les formes linéaires en mécanique quantique.

\paragraph{mhchem.} Cette extension fournit une interface pour taper des formules chimiques brutes et des réactions simples. Elle a de nombreux substituts et équivalents.

\section{Tableaux}

\paragraph{multirow.} Permet de fusionner des cellules sur plusieurs lignes dans un tableau.

\paragraph{booktabs.} Les tableaux \LaTeX{} de base sont assez laids. booktabs aide à les rendre plus élégants. Les commandes de booktabs sont incluses dans Memoir.

\paragraph{longtable.} Il peut être nécessaire de faire s'étaler un tableau sur plusieurs pages. L'extension longtable est là pour cela, tout simplement !

\paragraph{pgfplotstables.} Cette extension, fournie avec pgfplots, permet la mise en forme de tableaux à partir de données au format CSV.

\section{Bibliographie et nomenclature}

\paragraph{biblatex.} Rien d'autre que biblatex n'est nécessaire à la composition d'une bibliographie de qualité. Cette extension permet une personnalisation avancée de la bibliographie et des citations. Il est également possible de faire des bibliographies par chapitre, par section, de faire des extraits bibliographiques, etc. Elle supporte des commandes de compatibilité avec natbib et interagit automatiquement avec babel. biblatex est incompatible avec de nombreuses extensions, et la liste est fournie dans son excellent manuel. biblatex est destinée à être utilisée avec biber plutôt que bib\TeX. Attention : l'utilisation de cette extension avec la classe Memoir peut nécessiter certaines précautions (voir le manuel).

\paragraph{nomencl.} Écrire une nomenclature peut être compliqué. On peut le faire à la main avec l'extension longtable, mais on peut également le faire au moyen de l'extension nomencl. Elle permet alors de déclarer les éléments de la nomenclature dans le désordre et les classera automatiquement à la compilation. On notera que l'usage de l'outil makeindex est nécessaire pour compiler une nomenclature réalisée avec nomencl. La nomenclature est composée au moyen d'un environnement de type liste. Une variante de cette extension, nommée nomentbl, compose la nomenclature au moyen d'un environnement \texttt{longtable}. Il est à noter que le fichier \texttt{nomencl.ist}, présent dans l'arborescence de la distribution \TeX{}, n'est souvent pas trouvé à la compilation et qu'il peut être plus confortable de le copier à la racine de l'arborescence des sources du document.

\section{Liens hypertextes}

\paragraph{hyperref.} hyperref permet l'inclusion de liens dans les documents PDF, facilitant ainsi la navigation dans le document. Cette extension est \enquote{obligatoire}, mais c'est également un véritable cauchemar pour ce qui est de la gestion des conflits. Heureusement, cher lecteur, peu de conflits existent avec les recommendations que je fais ici. Les conflits entre hyperref et Memoir sont automatiquement gérés, et biblatex et hyperref interagissent sans problème. Si tu rencontres des difficultés, je serai content de le savoir. Par mesure de sûreté, je préfère charger hyperref après toutes les extensions pouvant interagir avec elle (bien que cela ne soit pas toujours préférable ; c'est à voir au cas par cas). La couleur des liens et leur format peuvent être personnalisés.

\section{Figures}

\paragraph{graphicx.} Cette extension permet l'inclusion des graphiques au format PNG, JPEG ou PDF (du moins, lorsqu'on compile avec PDF\LaTeX{}).

\paragraph{tikz.} Cette extension est quasiment un langage de programmation à part entière. TikZ permet la création de graphiques vectoriels directement dans \LaTeX{}. Un intérêt majeur est qu'il permet l'inclusion de texte, de fait parfaitement intégré dans le document.

\paragraph{PGFPlots.} Cette extension utilise PGF, le moteur de rendu de TikZ, pour produire des graphes directement dans \LaTeX{}. Là encore, un intérêt majeur est que les fontes des légendes et des axes seront parfaitement intégrées au document.

% \section{Pour en savoir plus}

% Sur les fontes : voir le \href{http://www.tug.dk/FontCatalogue/}{\LaTeX{} Font Catalog}.